const mongoose = require("mongoose");

const diceHistoryModel = require("../models/diceHistoryModel");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const prizeModel = require("../models/prizeModel");
const userModel = require("../models/userModel");
const voucherHistoryModel = require("../models/voucherHistoryModel");
const voucherModel = require("../models/voucherModel");

const diceHandleplayer = (request,response) => {
    // B1: Chuẩn bị dữ liệu
    let username = request.body.username;
    let firstname = request.body.firstname;
    let lastname = request.body.lastname;

    let dice = Math.floor(Math.random()* 6 + 1);

    //B2: validate dữ liệu từ request body
    if (!username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Username is required"
        })
    }

    if (!firstname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Firstname is required"
        })
    }

    if (!lastname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Lastname is required"
        })
    }

    //sử dụng userModel.findone tìm kiếm username
    userModel.findOne({
        username: username
    }).catch((errorFindUser)=> {
        response.status(500).json({
            status:'Error 400: Bad request',
           message: `internal sever ${errorFindUser}`
        })
      })
      .then((dataUserExist) => {
          if(!dataUserExist){
            // Nếu user không tồn tại trong hệ thống
            // Tạo user mới
            userModel.create({
                username: username,
                firstname: firstname,
                lastname: lastname
            }).catch((errCreateUser)=> {
                response.status(500).json({
                   message: `internal sever ${errCreateUser}`
                })
              })
              .then((userCreated) => {
                // Xúc xắc 1 lần, lưu lịch sử vào Dice History
                  diceHistoryModel.create({
                    user: userCreated._id,
                    dice: dice
                  }).catch((errorDiceHistoryCreate)=> {
                    response.status(500).json({
                       message: `internal sever ${errorDiceHistoryCreate}`
                    })
                  })
                  .then((diceHistoryCreated) => {
                      if(dice <3){
                         // Nếu dice < 3, không nhận được voucher và prize gì cả
                        return response.status(200).json({
                            dice: dice,
                            prize: null,
                            voucher: null
                        })
                      }else{
                        // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                        voucherModel.count().catch((error)=> {
                            response.status(500).json({
                               message: `internal sever ${error}`
                            })
                          })
                          .then((countVoucher) => {
                              let ramdomVoucher = Math.floor(Math.ramdom * countVoucher);
                              voucherModel.findOne().skip(ramdomVoucher)
                                .then((findVoucher) => {
                                     // Lưu voucher History
                                    voucherHistoryModel.create({
                                        user: userCreated._id,
                                        voucher: findVoucher._id
                                    }).catch((errorCreateVoucherHistory)=> {
                                        response.status(500).json({
                                           message: `internal sever ${error}`
                                        })
                                      })
                                      .then((voucherHistoryCreated) => {
                                        return response.status(201).json({
                                            dice: dice,
                                            prize: null,
                                            voucher: findVoucher
                                          })
                                       })
                                })
                           })
                      }
                   })
               })
          }
          else{
             // Nếu user đã tồn tại trong hệ thống
             // Xúc xắc 1 lần, lưu lịch sử vào Dice History
             diceHistoryModel.create({
                user: dataUserExist._id,
                dice: dice
             }).catch((errorDiceHistoryCreate)=> {
                    if(errorDiceHistoryCreate){
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errorDiceHistoryCreate.message
                        })
                    }else{
                        if(dice < 3){
                            return response.status(200).json({
                                dice: dice,
                                prize: null,
                                voucher: null
                            })
                        }else{
                            // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                            voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                let randomVoucher = Math.floor(Math.random * countVoucher);

                                voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                    // Lưu voucher History
                                    voucherHistoryModel.create({
                                        _id: mongoose.Types.ObjectId(),
                                        user: userExist._id,
                                        voucher: findVoucher._id
                                    }, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                        if (errorCreateVoucherHistory) {
                                            return response.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: errorCreateVoucherHistory.message
                                            })
                                        } else {
                                            // Lấy 3 lần gieo xúc xắc gần nhất của user
                                            diceHistoryModel
                                                .find()
                                                .sort({
                                                    _id: -1
                                                })
                                                .limit(3)
                                                .exec((errorFindLast3DiceHistory, last3DiceHistory) => {
                                                    if (errorFindLast3DiceHistory) {
                                                        return response.status(500).json({
                                                            status: "Error 500: Internal server error",
                                                            message: errorFindLast3DiceHistory.message
                                                        })
                                                    } else {
                                                        // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                        if (last3DiceHistory.length < 3) {
                                                            return response.status(200).json({
                                                                dice: dice,
                                                                prize: null,
                                                                voucher: findVoucher
                                                            })
                                                        } else {
                                                            console.log(last3DiceHistory)
                                                            // Kiểm tra 3 dice gần nhất
                                                            let checkHavePrize = true;
                                                            last3DiceHistory.forEach(diceHistory => {
                                                                if (diceHistory.dice < 3) {
                                                                    // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được giải thưởng
                                                                    checkHavePrize = false;
                                                                }
                                                            });

                                                            if (!checkHavePrize) {
                                                                return response.status(200).json({
                                                                    dice: dice,
                                                                    prize: null,
                                                                    voucher: findVoucher
                                                                })
                                                            } else {
                                                                // Nếu đủ điều kiện nhận giải thưởng, tiến hành lấy random 1 prize trong prize Model
                                                                prizeModel.count().exec((errorCountPrize, countPrize) => {
                                                                    let randomPrize = Math.floor(Math.random * countPrize);

                                                                    prizeModel.findOne().skip(randomPrize).exec((errorFindPrize, findPrize) => {
                                                                        // Lưu prize History
                                                                        prizeHistoryModel.create({
                                                                            _id: mongoose.Types.ObjectId(),
                                                                            user: userExist._id,
                                                                            prize: findPrize._id
                                                                        }, (errorCreatePrizeHistory, voucherPrizeCreated) => {
                                                                            if (errorCreatePrizeHistory) {
                                                                                return response.status(500).json({
                                                                                    status: "Error 500: Internal server error",
                                                                                    message: errorCreatePrizeHistory.message
                                                                                })
                                                                            } else {
                                                                                // Trả về kết quả cuối cùng
                                                                                return response.status(200).json({
                                                                                    dice: dice,
                                                                                    prize: findPrize,
                                                                                    voucher: findVoucher
                                                                                })
                                                                            }
                                                                        })
                                                                    })
                                                                })
                                                            }
                                                        }
                                                    }
                                                })
                                        }
                                    })
                                })
                            })
                        }
                    }
              })
              .then((diceHistoryCreated) => {
                  response.status(201).json({
                     message: 'Successful to show all diceHistory by UserId',
                     diceHistory: diceHistoryCreated
                  })
               })

          }
       })

}

const getDiceHistoryByUsername = (request,response) => {
    const username = request.query.username

    userModel.findOne({
        username : username   
    }).catch((errorData)=> {
        response.status(500).json({
           message: `internal sever ${errorData}`
        })
      })
      .then((dataUser) => {
       if(dataUser){
        const userId = dataUser._id;
        const condition = {};
        condition.user = {$eq: userId};
        // console.log(userId);
            diceHistoryModel.find(condition)
            .then((dataDiceHistory) => {
                 response.status(201).json({
                   message: 'Successful to show all diceHistory by user',
                   diceHistory: dataDiceHistory
                })
             })
       }
       else{
        return response.status(201).json({
            message: 'Successful to show all diceHistory',
            diceHistory: dataUser
         })
       }
      
     })
}

const getPrizeHistoryByUsername = (request,response) => {
    const username = request.query.username

    userModel.findOne({
        username : username   
    }).catch((errorData)=> {
        response.status(500).json({
           message: `internal sever ${errorData}`
        })
      })
      .then((dataUser) => {
       if(dataUser){
        const userId = dataUser._id;
        const condition = {};
        condition.user = {$eq: userId};
        // console.log(userId);
            prizeHistoryModel.find(condition)
            .then((dataPrizeHistory) => {
                 response.status(201).json({
                   message: 'Successful to show all prizeHistory by user',
                   dataPrizeHistory: dataPrizeHistory
                })
             })
       }
       else{
        return response.status(201).json({
            message: 'Successful to show all diceHistory',
            diceHistory: dataUser
         })
       }
      
     })
}

const getVoucherHistoryByUsername = (request,response) => {
    const username = request.query.username

    userModel.findOne({
        username : username   
    }).catch((errorData)=> {
        response.status(500).json({
           message: `internal sever ${errorData}`
        })
      })
      .then((dataUser) => {
       if(dataUser){
        const userId = dataUser._id;
        const condition = {};
        condition.user = {$eq: userId};
        // console.log(userId);
            voucherHistoryModel.find(condition)
            .then((dataPrizeHistory) => {
                 response.status(201).json({
                   message: 'Successful to show all vouchcerHistory by user',
                   vouchcerHistory: vouchcerHistory
                })
             })
       }
       else{
        return response.status(201).json({
            message: 'Successful to show all diceHistory',
            vouchcerHistory: dataUser
         })
       }
      
     })
}
module.exports = {
    diceHandleplayer,
    getDiceHistoryByUsername,
    getPrizeHistoryByUsername,
    getVoucherHistoryByUsername
}