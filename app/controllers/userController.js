// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Users Model
const userModel = require('../models/userModel');

const getAllUser = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    userModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all user',
         product: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnUserById = (request,response) => {
    //b1: thu thập dữ liệu
    const userId = request.params.userId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${userId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    userModel.findById(userId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!body.username) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }

    if(!body.firstname) {
        return response.status(400).json({
            status: "Bad Request",
            message: "firstname không hợp lệ"
        })
    }

    if(!(body.lastname) ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newUser = {
       
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname
    }

    userModel.create(newUser)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newUser',
             productType: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if(body.username !== undefined && body.username.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "name không hợp lệ"
        })
    }

    if(body.firstname !== undefined && body.firstname.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "firstname không hợp lệ"
        })
    }
    if(body.lastname !== undefined && body.lastname.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "lastname không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updateUser = {
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
         updatedAt: vCurrentTime
    }

    

    userModel.findByIdAndUpdate(userId,updateUser)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new user',
             User: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteUserByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.userId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "userId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findByIdAndDelete(userId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an user ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteUserByID,
    updateUserById,
    createUser,
    getAnUserById,
    getAllUser
}