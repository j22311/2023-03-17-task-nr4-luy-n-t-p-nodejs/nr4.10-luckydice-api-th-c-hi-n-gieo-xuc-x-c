//khai báo thư viện 
const express = require('express');
const { diceHandleplayer } = require('../controllers/diceContronller');
const { getAllUser, getAnUserById, createUser, updateUserById, deleteUserByID } = require('../controllers/userController');

//khai báo một biến ROuter chạy trên file index.js
const userRouter = express.Router();

userRouter.get('/users/',getAllUser);

userRouter.get('/users/:userId',getAnUserById);

userRouter.post('/users/',createUser);
//userRouter.post('/devcamp-lucky-dice/dice',diceHandleplayer);
// userRouter.post('/users/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

userRouter.put('/users/:userId',updateUserById);

userRouter.delete('/users/:userId',deleteUserByID);

module.exports = {
    userRouter
}