const express = require('express');
const { diceHandleplayer, getDiceHistoryByUsername, getPrizeHistoryByUsername, getVoucherHistoryByUsername,  } = require('../controllers/diceContronller');

const diceRouter = express.Router();

diceRouter.post('/devcamp-lucky-dice/dice',diceHandleplayer);

diceRouter.get('/devcamp-lucky-dice/dice-history',getDiceHistoryByUsername);

diceRouter.get('/devcamp-lucky-dice/prize-history',getPrizeHistoryByUsername);

diceRouter.get('/devcamp-lucky-dice/voucher-history',getVoucherHistoryByUsername);

module.exports = {
    diceRouter
}