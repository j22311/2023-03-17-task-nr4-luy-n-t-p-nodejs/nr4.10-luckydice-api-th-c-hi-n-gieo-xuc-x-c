//khai báo thư viện 
const express = require('express');
const { getAllVoucherHistory, getAVoucherHistoryById, createVoucherHistory, updateVoucherHistoryById, deleteVoucherHistoryByID, getAllVoucherHistoryByUserId } = require('../controllers/voucherHistoryController');

//khai báo một biến ROuter chạy trên file index.js
const voucherHistoryRouter = express.Router();

voucherHistoryRouter.get('/voucherHistorys/',getAllVoucherHistory);

voucherHistoryRouter.get('/voucherHistorys/uer/:userId',getAllVoucherHistoryByUserId);

voucherHistoryRouter.get('/voucherHistorys/:voucherHistoryId',getAVoucherHistoryById);

voucherHistoryRouter.post('/voucherHistorys/',createVoucherHistory);
// voucherHistoryRouter.post('/voucherHistorys/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

voucherHistoryRouter.put('/voucherHistorys/:voucherHistoryId',updateVoucherHistoryById);

voucherHistoryRouter.delete('/voucherHistorys/:voucherHistoryId',deleteVoucherHistoryByID);

module.exports = {
    voucherHistoryRouter
}