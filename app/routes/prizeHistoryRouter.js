//khai báo thư viện 
const express = require('express');
const { getAllPrizeHistory, getAPrizeHistoryById, createPrizeHistory, updatePrizeHistoryById, deletePrizeHistoryByID, getAllPrizeHistoryByUserId } = require('../controllers/prizeHistoryController');

//khai báo một biến ROuter chạy trên file index.js
const prizeHistoryRouter = express.Router();

prizeHistoryRouter.get('/prizeHistorys/',getAllPrizeHistory);

prizeHistoryRouter.get('/prizeHistorys/user/:userId',getAllPrizeHistoryByUserId);

prizeHistoryRouter.get('/prizeHistorys/:prizeHistoryId',getAPrizeHistoryById);

prizeHistoryRouter.post('/prizeHistorys/',createPrizeHistory);
// prizeHistoryRouter.post('/prizeHistorys/', (req,res) => {
//     const body = req.body
//     res.json({
//         data: body
//     });
// })

prizeHistoryRouter.put('/prizeHistorys/:prizeHistoryId',updatePrizeHistoryById);

prizeHistoryRouter.delete('/prizeHistorys/:prizeHistoryId',deletePrizeHistoryByID);

module.exports = {
    prizeHistoryRouter
}